from pyramid.view import view_config
import rasoi.utils



def extend_model(model_dict):
    """
    Addes the hashing function for cache busting to model dictionary
    :param model_dict:
    :return: model_dict: but with one additional key which holds the hashing function
    """
    model_dict["build_cache_id"]=rasoi.utils.build_cache_id
    return model_dict

'''
@view_config(route_name='home', renderer='templates/albums.pt')
def index(request):
    albums = [
        {'has_preview': True, 'title': 'Digital ...',  'url': '/album/123'},
        {'has_preview': True, 'title': 'Freedom songs',  'url': '/album/993'},
        {'has_preview': False, 'title': 'Making money',  'url': '/album/722'}
    ]
    return {'albums': albums}


@view_config(route_name='home', renderer='templates/mytemplate.pt')
def myfun(request):
    return({'app':'app'})
'''